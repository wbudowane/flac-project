################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Core/Components/cs43l22 \
Core/Components/lis302dl \
Core/Components/lis3dsh \
Core/Src \
Core/Src/src/flac \
Core/Src/src/libFLAC \
Core/Src/src/plugin_common \
Core/Startup \
Drivers/BSP/STM32F4-Discovery \
Drivers/STM32F4xx_HAL_Driver/Src \
FATFS/App \
FATFS/Target \
Middlewares/ST/STM32_USB_Host_Library/Class/MSC/Src \
Middlewares/ST/STM32_USB_Host_Library/Core/Src \
Middlewares/Third_Party/FatFs/src \
Middlewares/Third_Party/FatFs/src/option \
Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS \
Middlewares/Third_Party/FreeRTOS/Source \
Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F \
Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang \
USB_HOST/App \
USB_HOST/Target \

