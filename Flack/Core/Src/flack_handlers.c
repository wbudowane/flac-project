#include "FLAC/stream_decoder.h"
#include "ff.h"
#include "cs43l22.h"
#include "stm32f4_discovery_audio.h"
#include <string.h>
#include <stdbool.h>
#include "dbgu.h"
#include "term_io.h"
#include "main.h"
#include <math.h>
#include <string.h>
#include "cmsis_os.h"

#define AUDIO_SIZE 32768/2
#define BSP_sample_bits 16
#define BSP_sample_bytes (BSP_sample_bits/8)

typedef struct {
    unsigned sample_rate;
    unsigned channels;
    unsigned bits_per_sample;
    uint64_t total_samples;
} MetaFlac; //names and use corresponding with data from flaclib

typedef struct {
    uint8_t *buffer;
    int size;
    int samples;
} Frame; //hold data from single frame

typedef struct{
	FLAC__StreamDecoder *decoder;
	FIL* file;
	MetaFlac meta;
	Frame *frame;
}Flac_data;  //data needed for callbacks

typedef struct {
	Flac_data* flac;
    Frame* last_frame;
    size_t offset; //offset of last_farme
} ReadState; //struct to keep data of last read frame

static uint8_t buffer_audio[AUDIO_SIZE];
static volatile bool fulfill_buffer = false;
static volatile bool fill_front = true;

static bool is_playing = false;
static bool finished = true;
static uint64_t played_part; //to display on lcd
static FIL current_file;
static ReadState read_state;
static Flac_data* flac_data;
static MetaFlac meta;

static int /*to return size or error (negative)*/ read_from_file(FIL *file, void *buf, int len) {
	size_t bytes_readed;
    FRESULT res = f_read(file, buf, (size_t) len, &bytes_readed);

    if (res != FR_OK)
        return -1;

    return (int) bytes_readed; //cast to match returned type
}

static FLAC__StreamDecoderReadStatus DecoderReadCallback( const FLAC__StreamDecoder *decoder,
 FLAC__byte *buffer, size_t *bytes, void *client_data) { //return inf
	Flac_data *flac = (Flac_data *) client_data;

    if (*bytes <= 0) //trying to read data after end or abort
        return FLAC__STREAM_DECODER_READ_STATUS_ABORT;

    int readed_bytes = read_from_file(flac->file, buffer, *bytes); //int for negative if error

    if (readed_bytes > 0) {
        *bytes = (size_t) readed_bytes; //cast to compatible size
        return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
    } else if (readed_bytes == 0) {
        *bytes = 0;
        return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;
    } else {
        *bytes = 0;
        return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
    }
}

static FLAC__StreamDecoderWriteStatus DecoderWriteCallback(const FLAC__StreamDecoder *decoder,
 const FLAC__Frame *frame, const FLAC__int32 *const *buffer, void *client_data) {
	Flac_data *flac = (Flac_data *) client_data;

    for (int i = 0; i < frame->header.channels; i++) //first data in buffer should inform about channels
        if (buffer[i] == NULL)
            return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;

    int samples = frame->header.blocksize;
    int channels = frame->header.channels;
    int shift_bit = frame->header.bits_per_sample - BSP_sample_bits;
    int size = samples * channels * BSP_sample_bytes;

    flac->frame = (Frame*)malloc(sizeof(Frame)); //allocate memory for frame
    *flac->frame = (Frame) { //set frame, allocate memory for buffer and set size
        .size = size,
        .buffer = (uint8_t*)malloc(size),
		.samples = samples
    };

    if(flac->frame->buffer==0){
    	xprintf("critical error no memory for buffer in frame required %d\n",size);
    	return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
    }

    for (int sample = 0; sample < samples; sample++) { 
        //buffer linearization with transposition and sample scaling
    	for (int channel = 0; channel < channels; channel++) {
    		for (int byte = 0; byte < BSP_sample_bytes; byte++) {
    			if(byte * 8 + shift_bit>0)
    				flac->frame->buffer[(sample * channels + channel) * BSP_sample_bytes + byte] =
                     (uint8_t)((buffer[channel][sample] >> (byte * 8 + shift_bit)) & 0xFF);
    			else
    				flac->frame->buffer[(sample * channels + channel) * BSP_sample_bytes + byte] = 
                    (uint8_t)((buffer[channel][sample] << -(byte * 8 + shift_bit)) & 0xFF);
            }
        }
    }

    return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

static void DecoderMetadataCallback(const FLAC__StreamDecoder *decoder, const FLAC__StreamMetadata *metadata,
 void *client_data) { //save meta data
	Flac_data *flac = (Flac_data *) client_data;

    if (metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
        flac->meta = (MetaFlac) {
            .total_samples = metadata->data.stream_info.total_samples,
            .sample_rate = metadata->data.stream_info.sample_rate,
            .channels = metadata->data.stream_info.channels,
            .bits_per_sample = metadata->data.stream_info.bits_per_sample
        };
    }
}

static void DecoderErrorCallback(const FLAC__StreamDecoder *decoder,FLAC__StreamDecoderErrorStatus status,void *client_data) {
    ;//xprintf("error : %d\n",status);//TODO handle error here
}

void clear_flac_data(Flac_data* flac_data){//destroy Flack_data structure with it's content
    if (flac_data != NULL) {
        if (flac_data->decoder != NULL)
            FLAC__stream_decoder_delete(flac_data->decoder);
        free(flac_data);
    }
}

Flac_data *init_flac(FIL *file) {//init falc_data structure and native FLAC decoder and stream
	Flac_data *flac = calloc(1,sizeof(Flac_data));//create structure
    flac->file=file;//set data source
    flac->decoder = FLAC__stream_decoder_new();//create and set new native decoder
    if (flac->decoder == NULL) {//check if decoder created properly
    	clear_flac_data(flac);
        return NULL;
    }

    FLAC__stream_decoder_set_md5_checking(flac->decoder, false);//setting decoder options

    FLAC__StreamDecoderInitStatus init_status = FLAC__stream_decoder_init_stream(//init decoder status
        flac->decoder,//set decoder
        &DecoderReadCallback,
        NULL,//seek,tell,length,eof callback not set (do not need to be)
        NULL,
        NULL,
        NULL,
        &DecoderWriteCallback,
        &DecoderMetadataCallback,
        &DecoderErrorCallback,
        flac//pass structure to initialize
    );
    if (init_status != FLAC__STREAM_DECODER_INIT_STATUS_OK) {//check if initialized properly
    	clear_flac_data(flac);
        return NULL;
    }

    return flac;
}

bool read_meta(Flac_data *flac, MetaFlac *meta) {//read meta data and set meta struct, return true if operation completed successfully
    if (FLAC__stream_decoder_process_until_end_of_metadata(flac->decoder)) {//pass meta to decoder
        *meta = flac->meta;
        return true;
    }
    return false;
}

bool read_frame(Flac_data *flac, Frame **frame) {//read single frame and put it to send argument
    if (FLAC__stream_decoder_process_single(flac->decoder)/*read frame*/) {
        if (flac->frame == NULL) //check if frame loaded correctly
            return false; //return error info

        *frame = flac->frame; //set response frame
        flac->frame = NULL; // clear frame in Flac_data struct
        return true; //return success info
    }
    return false;//return error info

}

void clear_frame(Frame *frame) {//free memory of one frame
	if(frame!=NULL){
		free(frame->buffer);
		free(frame);
	}
}

ReadState init_state(Flac_data *flac) { //set begin state for reading
    return (ReadState) {
        .flac = flac, //associate Flac_data
        .last_frame = NULL, //set no last frame
        .offset = 0 //reset offset of frame
    };
}

void clear_state(ReadState *state) {//clear state
	clear_frame(state->last_frame);
    *state = (ReadState) {
        .flac = NULL,
        .last_frame = NULL,
        .offset = 0
    };
}

int read_data(ReadState *state, void *dest, int size) {//read data to buffer from frames
    int written = 0;//amount of written data
    while (written < size) {//condition to check if available space in buffer
        if (state->last_frame == NULL) {//if last frame empty get new frame
        	state->offset = 0;
            if (!read_frame(state->flac, &state->last_frame)) {//if frame not read finish
                return written;
            }
        }

        int remaining_data_in_frame = state->last_frame->size - state->offset;//amount of bytes in frame
        int dest_space_left = size - written;//remaining data in buffer

        if (remaining_data_in_frame <= dest_space_left) {//entire frame fit in remaining buffer
            memcpy(dest + written, &state->last_frame->buffer[state->offset], remaining_data_in_frame);//writing to buffer
            written += remaining_data_in_frame;
            clear_frame(state->last_frame);//clear last frame
            state->last_frame = NULL;
            state->offset = 0;
        } else {//frame is bigger than space in buffer
            memcpy(dest + written, &state->last_frame->buffer[state->offset], dest_space_left);//writing to buffer
            written += dest_space_left;
            state->offset += dest_space_left;
        }
    }

    return written;
}



void BSP_AUDIO_OUT_HalfTransfer_CallBack(void) {
    fulfill_buffer = true;
    fill_front = true;
}

void BSP_AUDIO_OUT_TransferComplete_CallBack(void) {
    fulfill_buffer = true;
    fill_front = false;
}

void init_audio(void) {
    if (BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_HEADPHONE, 40, AUDIO_FREQUENCY_22K) != 0) {
       //xprintf("BSP init error"); //TODO error handling
    }
}

void stop(void);
uint64_t audio_percent(void);

void check_load(void) {
    if (is_playing) {
        if (fulfill_buffer) {
        	//xprintf("%s\n",FLAC__StreamDecoderStateString[FLAC__stream_decoder_get_state(flac_data->decoder)]);
        	//counter2++;
        	//xprintf("%d, %d\n",counter-counter2,(counter-counter2)*1000/counter);
        	//xprintf("b\n");
        	fulfill_buffer = false;
            int bytes_read = read_data(&read_state, &buffer_audio[fill_front?0:AUDIO_SIZE / 2], AUDIO_SIZE / 2);

            played_part += bytes_read / meta.channels / meta.bits_per_sample * 8;

            if (bytes_read < AUDIO_SIZE / 2) {
                stop();
            }
        }
    }
    //play next if finish == true
}

uint64_t audio_percent(void) {
    if (meta.total_samples == 0) {
        return 0;
    }
    uint64_t progress = played_part * 1000 / meta.total_samples;
    if (progress > 1000) {
        return 1000;
    }
    return progress;
}

bool start_new_song(const char *filename) {
    if(is_playing){
    	xprintf("song stil plays\n");
    	return false;
    }

    FRESULT res = f_open(&current_file, filename, FA_READ);
    if (res != FR_OK) {
    	xprintf("f_open err %s, %d\n",filename,res);
        return false;
    }

    flac_data = init_flac(&current_file);
    read_state = init_state(flac_data);

    if (!read_meta(flac_data, &meta)) {
    	xprintf("meta error\n");
    	return false;
    }

    BSP_AUDIO_OUT_SetFrequency(meta.sample_rate);
    xprintf("sample rate %d\n",meta.sample_rate);

    int bytes_read = read_data(&read_state, buffer_audio, AUDIO_SIZE / 2);
    xprintf("bytes read: %d\n",bytes_read);
    if (bytes_read < AUDIO_SIZE / 2) {
        stop();
        return false;
    }

    fill_front = false;
    fulfill_buffer = true;

    BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_OFF);

	if(BSP_AUDIO_OUT_Play((uint16_t *) buffer_audio, AUDIO_SIZE)!=AUDIO_OK){
		//BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_ON);
		xprintf("error play bsp\n");
		return false;
	}
	else
		finished = false;

	BSP_AUDIO_OUT_SetVolume(40);
	is_playing = true;
	return true;
//    if(BSP_AUDIO_OUT_Resume()!=AUDIO_OK)
//        xprintf("error resume bsp\n");
}

void pause(void) {
    if(!is_playing){
        //TODO handle error
    }
    xprintf("pause\n");
    is_playing = false;
    BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_ON);
    BSP_AUDIO_OUT_Pause();
}

void resume(void) {
    if(is_playing){
        //TODO handle error
    }
    xprintf("resume\n");
    is_playing = true;
    BSP_AUDIO_OUT_Resume();
    BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_OFF);
}

void stop(void) {
	BSP_AUDIO_OUT_SetMute(AUDIO_MUTE_ON);

    is_playing = false;
    finished = true;

    BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);

    clear_state(&read_state);
    clear_flac_data(flac_data);
    flac_data = NULL;

    f_close(&current_file);

    played_part = 0;
}

FILINFO fil;
DIR d;

char file_path[_MAX_LFN + 4] = "0:/";

bool is_flac(char* name){
	char size = strlen(name);
	if(size<5)
		return false;
	return strcmp(name+size-5,".flac")==0;
}

char* names[256];
int file_num = 0;
int current = 0;

void find_file(void){
	while (f_readdir(&d,&fil) == FR_OK) {
	  if(!fil.fname[0])
		  return;
	  else{
		  //xprintf("found file: %s\n", fil.fname);
		  if(is_flac(fil.fname)){
			  strcpy(file_path + 3,fil.fname);
			  xprintf("found flac file: %s\n", file_path);
			  names[file_num] = malloc(strlen(file_path)+1);
			  strcpy(names[file_num++],file_path);
		  }
	  }
	}
}

GPIO_PinState pause_pressed = GPIO_PIN_RESET;
uint32_t last_tic_pause = 0;

GPIO_PinState next_pressed = GPIO_PIN_RESET;
uint32_t last_tic_next = 0;

GPIO_PinState prev_pressed = GPIO_PIN_RESET;
uint32_t last_tic_prev = 0;

void pause_button_check(){
	uint32_t cur = osKernelSysTick();
	if(cur-last_tic_pause>100){
		last_tic_pause=cur;
		GPIO_PinState st = HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_8);
		if(st != pause_pressed){
			pause_pressed = st;
			if(pause_pressed == GPIO_PIN_SET){
				if(is_playing)
					pause();
				else
					resume();
			}
		}
	}
}

void next_button_check(){
	uint32_t cur = osKernelSysTick();
	if(cur-last_tic_next>100){
		last_tic_next=cur;
		GPIO_PinState st = HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_9);
		if(st != next_pressed){
			next_pressed = st;
			if(next_pressed == GPIO_PIN_SET){
				stop();
			}
		}
	}
}

void prev_button_check(){
	uint32_t cur = osKernelSysTick();
	if(cur-last_tic_prev>100){
		last_tic_prev=cur;
		GPIO_PinState st = HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_7);
		if(st != prev_pressed){
			prev_pressed = st;
			if(prev_pressed == GPIO_PIN_SET){
				current-=2;
				if(current<0){
					current=file_num+current;

				}
				stop();
			}
		}
	}
	//xprintf("c:%d\n",current);
}

void test(void);

int save_current=0;

void send_string(char[]);
void cmd(uint8_t);
void character(uint8_t);

void send_song_name(){
	cmd(0x01);
	send_string(names[save_current]);
}

void sequence(void){
	test();
	int res = f_opendir(&d,"0:/");
	if (res != FR_OK) {
		xprintf("dir error\n");
	}
	find_file();
	for(;;){
		xprintf("play %s song\n",names[current]);
		start_new_song(names[current]);
		save_current = current++;
		send_song_name();
		if(current==file_num)
			current=0;
		while(!finished)
		{
			check_load();
			pause_button_check();
			next_button_check();
			prev_button_check();
		}
	}
}


//lcd

void set_rs(int val){
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_14,val);
}

void set_e(int val){
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_10,val);
}

void set_data(uint8_t data){
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,data & 1);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_15,data & 2);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_13,data & 4);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_15,data & 8);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_9,data & 16);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_11,data & 32);
	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_14,data & 64);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12,data & 128);
//	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_13,data & 128);
//	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_15,data & 64);
//	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_13,data & 32);
//	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_15,data & 16);
//	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_9,data & 8);
//	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_11,data & 4);
//	HAL_GPIO_WritePin(GPIOE,GPIO_PIN_14,data & 2);
//	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12,data & 1);
}

void cmd(uint8_t comend){
	set_data(comend);
	set_rs(0);
	set_e(1);
	osDelay(20);
	set_e(0);
	osDelay(20);
}

void character(uint8_t val){
	set_data(val);
	set_rs(1);
	set_e(1);
	osDelay(5);
	set_e(0);
	osDelay(5);
}

void init(void){
	osDelay(200);
	set_rs(0);
	set_e(0);
	osDelay(20);
//	cmd(0x30);
//	osDelay(2000);
//	cmd(0x30);
//	osDelay(200);
//	cmd(0x30);
//	osDelay(500);
	cmd(0x38);
	osDelay(100);
	cmd(0x38);
	osDelay(100);
	cmd(0x0C);
	cmd(0x01);
	cmd(0x06);
	//cmd(0x38);

	//cmd(0x0C);
	//cmd(0x0C);

	//cmd(0x30);
	//cmd(0x30);
	//cmd(0x30);

	//cmd(0x38);

	//cmd(0x08);

	//cmd(0x01);

	//cmd(0x04);

	//cmd(0x0F);

	//cmd(0x80);
}

void send_string(char str[]){
	for(int i=0;i<16;i++){
		if(!str[i])
			break;
		character(str[i]);
	}
}

//uint8_t my_sign[5][8] = {};

//void set_char(){
//	uint8_t t=16;
//	uint8_t s=8;
//	cmd(0x40+(0*8));
//	for(int j=0;j<5;j++){
//		for(int i=0;i<8;i++)
//			character(t);
//		t+=s;
//		s/=2;
//	}
//	cmd(0x80);
//}

void test(void){
	init();
	//uint8_t i=0;
//	while(i<120){
//		xprintf("%x\n",i);
//		cmd(i++);
//		character('a');
//	}
//	while(i<128){
//		character('a'+i++%26);
//		cmd(0x0C);
//		cmd(0x02);
//	}
//	while(i<128){
//		character('a'+i++%26);
//		//cmd(0x0C);
//		cmd(0x02);
//	}
	cmd(0x01);
	send_string("READY");
//	set_char();
//	for(int i=0x80;i<=0xFF;i++){
//		cmd(i);
//		character('a');
//	}
//	for(int i=0;i<5;i++)
//		character(i);
	//cmd(0x80);
	//send_string("READY");
	//for(;;);
}











